package pcd.ass01.ex2.sol;

import java.util.concurrent.Semaphore;

public class Output {
	
	private Semaphore mutex;
	
	public Output(){
		mutex = new Semaphore(1);
	}
	
	public void print(String msg){
		try {
			mutex.acquire();
			System.out.println(msg);
		} catch (Exception ex){
			ex.printStackTrace();
		} finally {
			mutex.release();
		}
	}

}
