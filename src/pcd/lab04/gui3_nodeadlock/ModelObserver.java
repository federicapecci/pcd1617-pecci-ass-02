package pcd.lab04.gui3_nodeadlock;

public interface ModelObserver {

	void modelUpdated(MyModel model);
}
