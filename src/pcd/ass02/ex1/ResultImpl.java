package pcd.ass02.ex1;

/**
 * Created by Federica on 18/04/17.
 */
public class ResultImpl implements Result{

    private long oracleValue;
    private long playerValue;

    public ResultImpl(final long oracleValue, final long playerValue){
        this.oracleValue = oracleValue;
        this.playerValue = playerValue;
    }


    @Override
    public boolean found() {
        return oracleValue == playerValue;
    }

    @Override
    public boolean isGreater() {
        return oracleValue > playerValue;
    }

    @Override
    public boolean isLess() {
        return oracleValue < playerValue;
    }
}
