package pcd.ass02.ex1;

import java.util.Random;

/**
 * Created by Federica on 18/04/17.
 * the main class creates Oracle e 9 Players who have to
 * guess number predicted by Oracle
 */
public class Main {

    public static int PLAYERS_NUMBER = 10;

    public static void main(String[] args) {

        OracleInterface oracle = OracleImpl.getIstance();

        for(int i=0; i<PLAYERS_NUMBER; i++) {
            Player player = new Player(i, oracle);
            System.out.println("Created player " + i);
            player.start();
        }
    }
}
