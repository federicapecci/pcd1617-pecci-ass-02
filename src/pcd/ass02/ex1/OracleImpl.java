package pcd.ass02.ex1;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Federica on 18/04/17.
 */
public class OracleImpl implements OracleInterface{

    private long magicNumber;
    private boolean isGameRunning;
    private static OracleImpl SINGLETON = null;
    private volatile Set<Integer> players;

    public static OracleImpl getIstance(){
        if(SINGLETON == null){
            synchronized (OracleImpl.class) {
                if(SINGLETON == null){

                    Long oracleValue = ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE);
                    System.out.println("*** Oracle says... " + oracleValue +" ***");
                    SINGLETON = new OracleImpl(oracleValue);
                }
            }

        }
        return SINGLETON;
    }

    private OracleImpl(final long magicNumber){
          this.magicNumber = magicNumber;
          this.isGameRunning = true;
          this.players = new HashSet<>();
    }

    /**
     * method to retry the state of game
     * @return
     */
    @Override
    public synchronized boolean isGameFinished() {
        return this.isGameRunning;
    }

    /**
     * method to let a player try to
     * guess oracle number
     * @param playerId
     * @param value
     * @return
     * @throws GameFinishedException
     */
    @Override
    public synchronized Result tryToGuess(int playerId, long value) throws GameFinishedException {
        Result result = new ResultImpl(this.magicNumber, value);

        //aggiungo l'id del player alla lista di quelli che hanno giocato
        if (result.found()) {
            this.isGameRunning = false;
            throw new GameFinishedException();
        }
        return result;
    }

    public synchronized Set<Integer> getPlayers() {
        return players;
    }


    public synchronized void addPlayers(int playerId) {
        players.add(playerId);
        if(isTurnFinished()){
            System.out.println("Turn finished!");
            System.out.println("");
            players.clear();
        }
    }

    @Override
    public synchronized boolean isTurnFinished() {
        return players.size() == 10;
    }
}
