package pcd.ass02.ex1;

import java.util.Set;

public interface OracleInterface {

	boolean isGameFinished();
	
	Result tryToGuess(int playerId, long value) throws GameFinishedException;

	Set<Integer> getPlayers();

	void addPlayers(int player);

	boolean isTurnFinished();

	
}
