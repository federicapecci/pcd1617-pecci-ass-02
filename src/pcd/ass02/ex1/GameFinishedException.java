package pcd.ass02.ex1;

/**
 * this exception has been called when
 * one of 9 players guesses the oracle
 * number
 */
public class GameFinishedException extends Exception {

}
