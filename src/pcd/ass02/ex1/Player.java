package pcd.ass02.ex1;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Federica on 18/04/17.
 * this class implements behaviour for a player
 * and method "run" specifies what a player does
 */
public class Player extends Thread {

    private int id;
    private OracleInterface oracle;
    private long tryValue;
    private long lowerBound;
    private long upperBound;
    private Set<Long> numberGuessed = new HashSet<Long>();
    private boolean isWinner;

    public Player(final int id, final OracleInterface oracle){
        this.id = id;
        this.oracle = oracle;
        this.tryValue = ThreadLocalRandom.current().nextLong(Long.MAX_VALUE);
        this.lowerBound = 0;
        this.upperBound = Long.MAX_VALUE;
        this.isWinner = false;
    }

    @Override
    public void run() {


        Result res = null;
        System.out.println("(first attempt) Player " + id + " tries " + tryValue);
        try {
            res = oracle.tryToGuess(id, tryValue);
            this.numberGuessed.add(tryValue);
            if (res.isGreater()) {
                this.lowerBound = tryValue;
            } else if (res.isLess()) {
                this.upperBound = tryValue;
            }

            oracle.addPlayers(id);

        } catch (GameFinishedException e) {
            e.printStackTrace();
        }


        while (oracle.isGameFinished()){
            if(!oracle.getPlayers().contains(id)) {

                while (this.numberGuessed.contains(tryValue)) {
                    tryValue = ThreadLocalRandom.current().nextLong(this.lowerBound, this.upperBound);
                }

                try {
                    System.out.println("Player " + id + " tries " + tryValue + " (limite inferiore: " + lowerBound +
                            ", limite superiore " + upperBound + ")");

                    res = oracle.tryToGuess(id, tryValue);
                    this.numberGuessed.add(tryValue);

                    if (res.isGreater()) {
                        this.lowerBound = tryValue;

                    } else if (res.isLess()) {
                        this.upperBound = tryValue;
                    }
                    oracle.addPlayers(id);

                } catch (GameFinishedException e) {
                    System.out.println("Player " + id + " -> WIN!");
                    isWinner = true;
                }
            }
        }

        if(!isWinner) {
            System.out.println("Player " + id + " -> SOB!");
        }
    }
}


