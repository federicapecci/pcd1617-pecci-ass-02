package pcd.ass02.ex2;

import pcd.ass01.ex1.sol.*;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Federica on 19/04/17.
 */
public class MandelbrotService extends Thread{


    private ExecutorService executor;
    private MandelbrotSetImage set;
    private int poolSize;

    public MandelbrotService(int poolSize, MandelbrotSetImage set){
        this.set = set;
        this.executor = Executors.newFixedThreadPool(poolSize);
        this.poolSize = poolSize;
    }


    public synchronized void compute(int nIter){
        this.executor = Executors.newFixedThreadPool(poolSize);

        Set<Future<Boolean>> resultSet = new HashSet<>();
        int w = set.getWidth();
        int h = set.getHeight();
        double delta = set.getDelta();
        Complex center = set.getCenter();

        //un'istanza di MandelbrotColumnTask per ogni colonna
        for (int x = 0; x < w; x++ ){
            //assegno come task una colonna del set ad ogni callable
            Future<Boolean> res = executor.submit(new MandelbrotColumnTask(set, x, w, h, nIter, delta, center));
            resultSet.add(res);
        }


        for (Future<Boolean> future : resultSet) {
            try {
                future.get();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }

        executor.shutdown();

    }


}
