package pcd.ass02.ex2;

import pcd.ass01.ex1.sol.*;
import pcd.lab05.executors.quad3_withfutures.IFunction;

import java.util.concurrent.Callable;


/**
 * Created by Federica on 19/04/17.
 * this class assign, as task to compute, a certain area (column) of a set
 */

public class MandelbrotColumnTask implements Callable<Boolean> {


    private MandelbrotSetImage image;
    private int xStart, xEnd, height, nIterMax;
    private double delta;
    private Complex center;

    public MandelbrotColumnTask(MandelbrotSetImage image, int xStart, int xEnd, int height,
                                int nIterMax, double delta, Complex center) {
        this.image = image;
        this.xStart = xStart;
        this.xEnd = xEnd;
        this.height = height;
        this.nIterMax = nIterMax;
        this.delta = delta;
        this.center = center;
    }


    public Boolean call() {
        for (int y = 0; y < height; y++) {
            double x0 = (xStart - xEnd * 0.5) * delta + center.re();
            double y0 = center.im() - (y - height * 0.5) * delta;
            double level = image.computeColor(x0, y0, nIterMax);
            int color = (int) (level * 255);
            image.getImage()[y * xEnd + xStart] = color + (color << 8) + (color << 16);
        }


        return true;
    }

    private void log(String msg) {
        System.out.println(msg);
    }
}


